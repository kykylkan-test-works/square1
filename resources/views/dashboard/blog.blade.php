@extends('layouts.app')

@section('content')
    <router-view></router-view>
@endsection

@section('js')
    @routes('blog')

    <script src="{{ asset('js/app.js') }}"></script>

    <script type="application/javascript">
        if (router.currentRoute.path == '/') {
            router.replace('/blog/list');
        }
    </script>
@endsection

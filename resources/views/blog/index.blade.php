@extends('layouts.home')

@section('content')
    <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
        @if($sort == 'asc')
            <strong>Sort by: Older</strong>
        @else
            <a href="{{ route('home').'?sort=asc' }}" class="underline text-gray-900 dark:text-white">Sort by: Older</a>
        @endif
        ||
        @if($sort == 'desc')
            <strong>Sort by: Newer</strong>
        @else
            <a href="{{ route('home').'?sort=desc' }}" class="underline text-gray-900 dark:text-white">Sort by: Newer</a>
        @endif
    </div>

    @foreach($posts as $post)
        <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
            <div class="grid grid-cols-1 md:grid-cols-2">
                <div class="p-6">
                    <div class="flex items-center">
                        <div class="ml-4 text-lg leading-7 font-semibold"><a href="#" class="underline text-gray-900 dark:text-white">{{ $post->title }}</a></div>
                    </div>

                    <div class="ml-12">
                        <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                            {{ $post->description }}
                        </div>
                    </div>
                    <p>Published at: {{ $post->published_at }}</p>
                    <p>Author: {{ $post->user->name }}</p>

                </div>
            </div>
        </div>
    @endforeach

    <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
        <div class="flex items-center" style="flex-direction: column;">
            <p>Pagination</p>
            {{ $posts->links() }}
        </div>
    </div>
@endsection

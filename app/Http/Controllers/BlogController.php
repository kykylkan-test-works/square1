<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\BlogRepositoryInterface;

/**
 * Class BlogController
 * @package App\Http\Controllers
 */
class BlogController extends Controller
{
    /**
     * @param BlogRepositoryInterface $blogRepository
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\View\View|\Laravel\Lumen\Application
     */
    public function index(BlogRepositoryInterface $blogRepository)
    {
        $sort = request()->sort;
        $posts = $blogRepository->getPublicPosts($sort);

        return view('blog.index', [
            'posts' => $posts,
            'sort' => $sort
        ]);
    }
}

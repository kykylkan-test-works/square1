<?php

namespace App\Jobs;

use App\Models\User;
use App\Services\BlogManager;
use App\Services\SyncManager;
use App\Traits\LogTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class SyncJob
 * @package App\Jobs
 */
class SyncJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, LogTrait;

    /**
     * @var User
     */
    protected $user;

    /**
     * SyncJob constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        $this->setCode(class_basename(__CLASS__));
    }

    /**
     * @param SyncManager $syncManager
     * @param BlogManager $blogManager
     */
    public function handle(SyncManager $syncManager, BlogManager $blogManager)
    {
        $result = $syncManager->getPosts();

        if($result === false) {
            return;
        }

        if (empty($result->data)) {
            $this->info('Data not found.', __FUNCTION__);
            return;
        }

        $posts = collect($result->data);

        if ($posts->isEmpty()) {
            $this->info('Data not found.', __FUNCTION__);
            return;
        }

        $blogManager->savePosts($this->user, $posts);
    }
}

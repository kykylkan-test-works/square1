<?php

namespace App\Repositories;

use App\Models\Post;
use App\Models\User;
use App\Repositories\Interfaces\BlogRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;


/**
 * Class DatabaseBlogRepository
 * @package App\Repositories
 */
class DatabaseBlogRepository extends BaseRepository implements BlogRepositoryInterface
{
    /**
     * DatabaseBlogRepository constructor.
     * @param Post $model
     */
    public function __construct(Post $model)
    {
        parent::__construct($model);
    }

    /**
     * @param int $postId
     * @return Post
     */
    public function getPostById(int $postId): Post
    {
        $query = $this->model->newModelQuery();

//        $query->with(['user']);

        return $query->findOrFail($postId);
    }

    /**
     * @param string $sort
     * @return LengthAwarePaginator
     */
    public function getPublicPosts($sort = 'desc'): LengthAwarePaginator
    {
        if ($sort === 'desc') {
//            $sort = Str::ucfirst($sort);
            $sort = 'Desc';
        } else {
            $sort = '';
        }

        $cacheKey = md5('posts-' . config('custom.per_page') . '-' . $sort . '-' . request()->page);

        // I suggest to use cache only if we have a lot of queries at same time and we will not have a super power server
        // TODO: Need to create a service for Caching
        if (!Cache::tags('posts')->has($cacheKey)) {
            Cache::tags('posts')->put($cacheKey, Cache::tags('posts_backup')->get($cacheKey), 60 * 60);

            $query = $this->model->newModelQuery()
                ->select(['id', 'title', 'description', 'published_at', 'user_id'])
                ->with('user:id,name')
                ->published()
                ->{"orderBy{$sort}"}('published_at');

            $data = Cache::tags('posts')->remember($cacheKey, 60 * 60, function () use ($query) {
                return $query->paginate(config('custom.per_page'))->withQueryString();
            });
            Cache::tags('posts_backup')->put($cacheKey, $data, (60 * 60) + 5);

            return $data;
        } else {
            return Cache::tags('posts')->get($cacheKey);
        }
    }

    /**
     * @param User $user
     * @return Collection
     */
    public function getUserPosts(User $user): Collection
    {
        $query = $user->posts()->orderByDesc('id');

        return $query->get();
    }

    /**
     * @param User $user
     * @param int $postId
     * @return Post
     */
    public function getPostForEdit(User $user, int $postId): Post
    {
        $query = $user->posts()->whereId($postId);

        return $query->first();
    }
}

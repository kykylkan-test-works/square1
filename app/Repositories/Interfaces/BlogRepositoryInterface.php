<?php

namespace App\Repositories\Interfaces;

use App\Models\Post;
use App\Models\User;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Interface BlogRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface BlogRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * @param int $postId
     * @return Post
     */
    public function getPostById(int $postId): Post;

    /**
     * @param string $sort
     * @return LengthAwarePaginator
     */
    public function getPublicPosts($sort = 'desc'): LengthAwarePaginator;

    /**
     * @param User $user
     * @return Collection
     */
    public function getUserPosts(User $user): Collection;

    /**
     * @param User $user
     * @param int $postId
     * @return Post
     */
    public function getPostForEdit(User $user, int $postId): Post;
}

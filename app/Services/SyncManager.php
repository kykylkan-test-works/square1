<?php

namespace App\Services;

use App\Exceptions\SyncException;
use App\Models\User;
use App\Traits\LogTrait;
use Http\Client\Exception\RequestException;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

/**
 * Class SyncManager
 * @package App\Services
 */
class SyncManager
{
    use LogTrait;

    /**
     * SyncManager constructor.
     */
    public function __construct()
    {
        $this->setCode(class_basename(__CLASS__));
    }

    /**
     * @return mixed|null
     */
    public function getPosts()
    {
        if(empty(config('custom.sq1_api_url'))) {
            $this->warning('Api url is empty!', __FUNCTION__);

            return false;
        }

        $response = Http::retry(3, 100, function ($exception) {
            return $exception instanceof ConnectionException;
        })->get(config('custom.sq1_api_url'));

        if ($response->failed()) {
            $this->error('Request was failed',
                __FUNCTION__,
                [
                    'response' => Str::substr($response->body(), 0, 200)
                ]
            );

            throw new SyncException('Request was failed');
        }

        $json = $response->body();

//        $json = '1'.$json;

        if (!Str::startsWith($json, '{"data":[')) {
            $this->error('Request was failed by wrong json',
                __FUNCTION__,
                [
                    'json' => Str::substr($json, 0, 200)
                ]
            );

            throw new SyncException('Request was failed by wrong json');
        }

        $data = json_decode($json);

        if (json_last_error()) {
            $this->error('Request was failed by wrong json',
                __FUNCTION__,
                [
                    'json' => Str::substr(json_last_error_msg(), 0, 200)
                ]
            );

            throw new SyncException('Request was failed by wrong json');
        }

        $this->info('Request was successful finished',
            __FUNCTION__,
            [
                'response' => Str::substr($json, 0, 200)
            ]
        );

        return $data;
    }
}

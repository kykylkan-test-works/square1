<?php

namespace App\Services;

use App\Models\Post;
use App\Models\User;
use App\Traits\LogTrait;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

/**
 * Class BlogManager
 * @package App\Services
 */
class BlogManager extends AbstractManager
{
    use LogTrait;

    /**
     * BlogManager constructor.
     */
    public function __construct()
    {
        $this->setCode(class_basename(__CLASS__));
    }

    /**
     * @param User $user
     * @param array $data
     * @return Post
     */
    public function createPost(User $user, array $data): Post
    {
        if (empty($data['slug'])) {
            $data['slug'] = Str::slug($data['title']);
        }

        $post = $user->posts()->create($data);

        return $post;
    }

    /**
     * @param $post
     * @param $data
     * @return mixed
     */
    public function updatePost(Post $post, $data): bool
    {
        $post->fill($data);

        $result = $post->save();

        return $result;
    }

    /**
     * @param $post
     * @return bool
     */
    public function deletePost(Post $post): bool
    {
        $result = $post->delete();

        return $result;
    }

    /**
     * @param User $user
     * @param array $posts
     */
    public function savePosts(User $user, Collection $posts): void
    {
        foreach ($posts as $post) {
            try {
                $post->published_at = $post->publication_date
                    ? Carbon::parse($post->publication_date)->format('Y-m-d H:i:s')
                    : null;

                $this->createPost($user, (array)$post);
                $this->info('Save the new post was successful',
                    __FUNCTION__,
                    [
                        'user_id' => $user->id,
                        'data' => $post
                    ]
                );
            } catch (Exception $e) {
                $this->warning('Save the new post was failed: ' . $e->getMessage(),
                    __FUNCTION__,
                    [
                        'user_id' => $user->id,
                        'data' => $post
                    ]
                );
            }
        }

        return;
    }
}

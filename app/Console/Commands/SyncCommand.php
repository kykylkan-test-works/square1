<?php

namespace App\Console\Commands;

use App\Jobs\SyncJob;
use App\Models\User;
use Illuminate\Console\Command;

class SyncCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:blog';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (empty(config('custom.sq1_api_url'))) {
            $this->warn('Api url is empty!');
            return Command::FAILURE;
        }

        $user = User::whereEmail('admin@example.com')->first();

        if (empty($user)) {
            $this->warn('User not found!');
            return Command::FAILURE;
        }

        SyncJob::dispatch($user);

        $this->info('Sync of blog was successful added to queue!');

        return Command::SUCCESS;
    }
}

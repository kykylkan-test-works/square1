<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\LogService;
use ErrorException;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (!empty(env('ERROR_REPORTING'))) {
            error_reporting(env('ERROR_REPORTING'));
        }

        // for preventing the broken pages on production
        set_error_handler([$this, 'handleError']);
    }

    /**
     * @throws ErrorException
     */
    public function handleError($level, $message, $file = '', $line = 0, $context = [])
    {
        if (env('APP_ENV') === 'production' && $level) {
            switch ($level) {
                case E_NOTICE:
                    LogService::notice($message, "$file:$line", 'N_PHP_NOTICE', $context);
                    break;
                case E_STRICT:
                    LogService::warning($message, "$file:$line", 'W_PHP_STRICT', $context);
                    break;
                case E_DEPRECATED:
                    LogService::warning($message, "$file:$line", 'W_DEPRECATED', $context);
                    break;
                case E_USER_DEPRECATED:
                    LogService::warning($message, "$file:$line", 'W_USER_DEPRECATED', $context);
                    break;
                case E_WARNING:
                    LogService::warning($message, "$file:$line", 'W_PHP_WARNING', $context);
                    break;
                default:
                    throw new ErrorException($message, 0, $level, $file, $line);
            }
        } else {
            throw new ErrorException($message, 0, $level, $file, $line);
        }
    }
}

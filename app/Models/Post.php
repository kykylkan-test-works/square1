<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Post
 * @package App\Models
 */
class Post extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    public $fillable = ['title', 'description', 'published_at', 'slug'];
    /**
     * @var string[]
     */
    public $hidden = ['user_id'];
    /**
     * @var string[]
     */
    public $dates = ['published_at'];
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param $q
     * @return mixed
     */
    public function scopePublished($q)
    {
        return $q->where('published_at', '<=', now());
    }

    /**
     * @param $slug
     */
    public function setSlugAttribute($slug)
    {
        if (static::whereSlug($slug)->exists()) {
            $slug = $this->incrementSlug($slug);
        }

        $this->attributes['slug'] = $slug;
    }

    /**
     * @param $slug
     * @return mixed|string
     */
    public function incrementSlug($slug)
    {
        $original = $slug;
        $count = 0;

        while (static::whereSlug($slug)->exists()) {
            $slug = "{$original}-" . ++$count;
        }

        return $slug;
    }
}

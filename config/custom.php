<?php

return [
    'per_page' => env('RECORDS_PER_PAGE'),
    'sq1_api_url' => env('SQ1_API_URL')
];

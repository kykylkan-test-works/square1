<?php

namespace Tests\Unit;

use App\Jobs\SyncJob;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Support\Facades\Bus;

class SyncBlogTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_sync_blog_command()
    {
        $this->seed();

        $this->artisan('sync:blog')
            ->assertExitCode(0);
    }

    public function test_sync_job()
    {
        Bus::fake();

        $this->seed();

        $user = User::whereEmail('admin@example.com')->first();

        SyncJob::dispatch($user);

        Bus::assertDispatched(SyncJob::class);
    }
}

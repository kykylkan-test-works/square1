<?php

namespace Tests\Feature;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreatePostsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_create_a_post()
    {
        $this->actingAs($user = User::factory()->create());

        $response = $this->post('/posts', [
            'title' => 'Testing title',
            'slug' => 'test-title',
            'description' => 'Testing desc',
            'published_at' => now()->format('Y-m-d H:i:s'),
        ]);

        $post = Post::first();

        $this->assertCount(1, Post::all());
        $this->assertEquals($user->id, $post->user_id);
        $this->assertEquals('Testing desc', $post->description);
        $this->assertEquals('Testing title', $post->title);
        $response->assertStatus(201)
            ->assertJson([
                'data' => [
                    'title' => $post->title,
                    'description' => $post->description,
                    'published_at' => $post->published_at->format('Y-m-d H:i'),
                ]
            ]);
    }

    /** @test */
    public function a_user_can_not_create_a_post()
    {
        $this->actingAs($user = User::factory()->create());

        $response = $this->json('post','/posts', [
            'title' => 'Testing title',
            'slug' => 'test-title',
//            'description' => 'Testing desc',
            'published_at' => now()->format('Y-m-d H:i:s'),
        ]);

        $response->assertStatus(422)
            ->assertJson([
                'errors' => ['description' => ["The description field is required."]]
            ]);
    }
}
